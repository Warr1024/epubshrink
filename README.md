# epubshrink

This application is intended to optimize collections of ebooks in epub format
for size, making them more efficient to store or transfer.

Mainly, this involves re-encoding images, which may be in obsolete formats
(WebP is mandated by epub 3+ and is more efficient than equivalent JPEG),
or store excessive quality (lossless encoding or high resolutions; it is
assumed that no image needs more than 1080p quality to be adequately
legible).

It is assumed in general that book collections you will run this against
are focused on textual content, and the images are accessory illustrations
or diagrams, e.g. this is not for high quality comic book collections
(which one would sanely have in another format like cbz).

### TODO

- Split into multiple code files
- Try to avoid recompression step
  - Validate real folder dirs and unpack files there
  - Use advzip to add files to zip directly so we can control compression per-file
  - Avoid compressing already-optimized webp files
- HTML Tidy optimization/cleanup
- Config options
  - Image compression options
  - Force recompression skipping clean checks

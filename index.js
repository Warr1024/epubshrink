"use strict";

const fs = require("fs");
const fsp = fs.promises;
const util = require("util");
const path = require("path");
const os = require("os");
const childproc = require("child_process");
const yauzl = require("yauzl");
const archiver = require("archiver");
const yauzlopen = util.promisify(yauzl.open);

const myname = "epubshrink";

const imagerx = new RegExp(".(?:gif|png|jpg|jpeg|webp)$", "i");
const textrx = new RegExp(".(?:xml|x?html?|opf|css|ncx)$", "i");

const earlyfiles = ["mimetype", "META-INF/container.xml", "OEBPS/content.opf"];

////////////////////////////////////////////////////////////////////////
// UTILITY

const rxesc = (s) => s.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");

const promqueue = (limit) => {
  limit = limit || os.cpus().length;
  const queue = [];
  let running = 0;
  const start = (job) =>
    (async () => {
      try {
        try {
          job.res(await job.act());
        } catch (e) {
          job.rej(e);
        }
      } finally {
        running--;
        checkstart();
      }
    })();
  const checkstart = () => {
    while (queue.length && running < limit) {
      const job = queue.shift();
      running++;
      start(job);
    }
  };
  const wrap = (act) => {
    let res, rej;
    const prom = new Promise((r, j) => {
      res = r;
      rej = j;
    });
    return {
      act,
      res,
      rej,
      prom,
    };
  };
  const push = (act) => {
    act = wrap(act);
    queue.push(act);
    checkstart();
    return act.prom;
  };
  push.push = push;
  push.unshift = (act) => {
    act = wrap(act);
    queue.unshift(act);
    checkstart();
    return act.prom;
  };
  return push;
};

const mysys = async (...args) => {
  const name = args.shift();
  const proc = childproc.spawn(name, args, {
    stdio: ["ignore", "inherit", "inherit"],
  });
  await new Promise((res, rej) => {
    proc.on("error", rej);
    proc.on("exit", (code, signal) =>
      signal
        ? rej(Error(`${name} signal ${signal}`))
        : code
        ? rej(Error(`${name} code ${code}`))
        : res()
    );
  });
};

////////////////////////////////////////////////////////////////////////
// QUEUES

const procq = promqueue();
const bookq = promqueue();

////////////////////////////////////////////////////////////////////////
// IMAGE OPTIMIZE

const webpdefines = {
  "alpha-compression": 1,
  "alpha-filtering": 2,
  "alpha-quality": 80,
  exact: false,
  "auto-filter": true,
  lossless: false,
  "low-memory": false,
  method: 6,
};
const convertopts = {
  quality: 80,
  resize: `${1920 * 1080}@>`,
};
const convertargs = [];
for (const [k, v] of Object.entries(convertopts))
  convertargs.push(`-${k}`, `${v}`);
for (const [k, v] of Object.entries(webpdefines))
  convertargs.push("-define", `webp:${k}=${v}`);

const imgconvert = async (entrymap, cachedir) => {
  await Promise.all(
    Object.values(entrymap)
      .filter((ent) => imagerx.test(ent.fileName))
      .map(async (ent) =>
        procq.push(async () => {
          const newfn = ent.fileName.replace(imagerx, `-${myname}.webp`);
          const newtmp = path.join(cachedir, encodeURIComponent(newfn));
          ent.imageReplace = {
            fileName: newfn,
            tempPath: newtmp,
          };
          await mysys(
            "convert",
            ent.tempPath,
            ...convertargs,
            ent.imageReplace.tempPath
          );
          const [pre, post] = await Promise.all([
            fsp.stat(ent.tempPath),
            fsp.stat(ent.imageReplace.tempPath),
          ]);
          if (post.size >= pre.size) delete ent.imageReplace;
          else
            console.warn(
              `${ent.fileName}: ${pre.size} -> ${post.size} (${
                Math.floor((post.size / pre.size) * 10000) / 100
              }%)`
            );
        })
      )
  );
};

////////////////////////////////////////////////////////////////////////
// UNPACK ORIGINAL

const unpack = async (fn, cachedir) => {
  const entrymap = {};
  const zip = await yauzlopen(fn, { lazyEntries: false, autoClose: false });
  zip.setMaxListeners(0);
  zip.on("entry", async (ent) => {
    entrymap[ent.fileName] = ent;
    ent.tempPath = path.join(cachedir, encodeURIComponent(ent.fileName));
  });
  await new Promise((res, rej) => {
    zip.on("error", rej);
    zip.on("end", res);
  });
  if (entrymap[`.${myname}`]) {
    console.warn(`${fn}: already shrunk`);
    return;
  }

  const seen = {};
  for (const name of Object.keys(entrymap)) {
    const base = path.basename(name).toLowerCase();
    if (seen[base]) throw Error(`duplicate entry name ${base}`);
    seen[base] = true;
  }

  const openread = util.promisify(zip.openReadStream.bind(zip));
  for (const ent of Object.values(entrymap)) {
    const rstr = await openread(ent);
    const wstr = fs.createWriteStream(ent.tempPath);
    rstr.pipe(wstr);
    await new Promise((res, rej) => {
      zip.on("error", rej);
      rstr.on("error", rej);
      wstr.on("error", rej);
      wstr.on("close", res);
    });
  }
  zip.close();

  return entrymap;
};

////////////////////////////////////////////////////////////////////////
// REPACK FINAL

const repack = async (fn, cachedir, entrymap) => {
  const ztmp = path.join(cachedir, `.${myname}-${Date.now()}.zip`);
  const arch = archiver("zip", { zlib: { level: 1 } });
  const wstr = fs.createWriteStream(ztmp);
  arch.pipe(wstr);

  const replacements = [];
  for (const ent of Object.values(entrymap))
    if (ent.imageReplace) {
      replacements.push([
        new RegExp(rxesc(path.basename(ent.fileName)), "gi"),
        path.basename(ent.imageReplace.fileName),
      ]);
      ent.fileName = ent.imageReplace.fileName;
      ent.tempPath = ent.imageReplace.tempPath;
    }

  const add = async (ent) => {
    if (ent.fileName === "mimetype")
      return arch.file(ent.tempPath, { name: ent.fileName, store: true });
    if (!textrx.test(ent.fileName))
      return arch.file(ent.tempPath, { name: ent.fileName });
    let text = await fsp.readFile(ent.tempPath, "utf-8");
    for (const [rx, repl] of replacements) text = text.replace(rx, repl);
    arch.append(text, { name: ent.fileName });
  };

  for (const n of earlyfiles) {
    const ent = entrymap[n];
    if (ent) await add(ent);
    delete entrymap[n];
  }
  for (const ent of Object.values(entrymap)) await add(ent);
  arch.append("\n", { name: `.${myname}` });
  arch.finalize();

  await new Promise((res, rej) => {
    arch.on("error", rej);
    wstr.on("error", rej);
    wstr.on("close", res);
  });

  await procq.push(async () => await mysys("advzip", "-qz4", ztmp));

  const fnnew = `${fn}.new`;
  const fnold = `${fn}.old`;
  const [pre, post] = await Promise.all([fsp.stat(fn), fsp.stat(ztmp)]);
  console.log(
    `${fn}: ${pre.size} -> ${post.size} (${
      Math.floor((post.size / pre.size) * 10000) / 100
    }%)`
  );

  await fsp.copyFile(ztmp, fnnew);
  await mysys("sync");
  await fsp.rename(fn, fnold);
  await fsp.rename(fnnew, fn);
  await mysys("sync");
};

////////////////////////////////////////////////////////////////////////
// PROCESS LOOP

const processepub = async (fn) => {
  const cachedir = await fsp.mkdtemp(
    path.join(os.tmpdir(), `.epubshrink-${process.pid}-`)
  );
  try {
    const entrymap = await unpack(fn, cachedir);
    if (!entrymap) return;
    await imgconvert(entrymap, cachedir);
    await repack(fn, cachedir, entrymap);
  } finally {
    await fsp.rm(cachedir, { recursive: true, force: true });
  }
};

const main = async () => {
  await Promise.all(
    process.argv
      .slice(2)
      .map(async (fn) => await bookq.push(async () => await processepub(fn)))
  );
};

main();
